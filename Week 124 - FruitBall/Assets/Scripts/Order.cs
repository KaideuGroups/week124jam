﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Order
{

    public List<Fruit> needs { get; private set; } = new List<Fruit>();
    public string customer { get; private set; }

    public Order(string customerName)
    {
        customer = customerName;
    }

    public void add(Fruit fruit)
    {
        needs.Add(fruit);
    }
}
