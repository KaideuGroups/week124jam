﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    [SerializeField]
    GameObject FruitObject;

    [SerializeField]
    GameObject BlenderObject;

    [SerializeField]
    Vector3 Window1;

    [SerializeField]
    Vector3 Window2;

    [SerializeField]
    Vector3 Window3;

    public int missedCount { get; private set; } = 0;

    [System.Serializable]
    public struct Fruits
    {
        public string name;
        public Sprite img;
    }

    public List<Fruits> fruits;

    public List<Order> orders { get; private set; } = new List<Order>();


    // Start is called before the first frame update
    void Start()
    {
        
        makeOrder();

        //LaunchFruit();
        //LaunchFruitOrders();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("q"))
            LaunchFruitOrders();
        if (Input.GetKeyDown("e"))
            LaunchFruit();
    }

    public void LaunchFruit()
    {
        Fruits info = fruits[Random.Range(0, fruits.Count)];
        GameObject nFruit = Instantiate(FruitObject, transform.position, Quaternion.identity);
        nFruit.GetComponent<Fruit>().setFruit(info.name, info.img);
        Debug.Log("Spawned: " + nFruit.GetComponent<Fruit>().type);
        nFruit.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -1) * 100);
        nFruit.AddComponent<PolygonCollider2D>();
    }

    public void LaunchFruitOrders()
    {
        Order o = orders[Random.Range(0, orders.Count)];
        Fruit f = o.needs[Random.Range(0, o.needs.Count)];
        GameObject nFruit = Instantiate(FruitObject, transform.position, Quaternion.identity);
        nFruit.GetComponent<Fruit>().setFruit(f.type, f.image);
        Debug.Log("Spawned: " + nFruit.GetComponent<Fruit>().type);
        nFruit.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -1) * 100);
        nFruit.AddComponent<PolygonCollider2D>();
    }

    public void makeOrder()
    {
        Order nOrder = new Order("TestCustomer");
        for (int i = 0; i < 1; i++)
        {
            Fruits info = fruits[Random.Range(0, fruits.Count)];
            Fruit nF = new Fruit();
            nF.setFruit(info.name, info.img);
            nOrder.add(nF);
        }
        GameObject nBlender = Instantiate(BlenderObject, Window1, Quaternion.identity);
        orders.Add(nOrder);
        nBlender.GetComponent<Blender>().myOrder = nOrder;
        Debug.Log("Order Needs: " + nBlender.GetComponent<Blender>().myOrder.needs[0].type);

    }

    public void ruinedOrder()
    {
        Debug.Log("Order ruined");
        missedCount++;
    }
}
