﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour
{
    public string type { get; private set; }
    public Sprite image { get; private set; }

    public void setFruit(string fruit, Sprite img)
    {
        type = fruit;
        if (img)
        {
            transform.GetComponent<SpriteRenderer>().sprite = img;
            image = img;
        }
            
    }
}
