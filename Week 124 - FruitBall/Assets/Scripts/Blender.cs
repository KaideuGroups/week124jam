﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blender : MonoBehaviour
{

    private Manager manager;
    public Order myOrder;

    private void Start()
    {
        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<Manager>();
    }

    private void Update()
    {
        if (myOrder.needs.Count <= 0)
        {
            complete();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Fruit"))
        {
            bool needed = false;
            Transform fruit = collision.transform;
            print("Fruit made it to blender");
            foreach (Fruit f in myOrder.needs)
                if (fruit.GetComponent<Fruit>().type == f.type)
                {
                    needed = true;
                    myOrder.needs.Remove(f);
                    break;
                }
            Destroy(fruit.gameObject);
            if (!needed)
            {
                manager.ruinedOrder();
                Destroy(gameObject);
            }
            UpdateOrder();
        }
    }

    private void complete()
    {
        Debug.Log("Order Completed");
        Destroy(gameObject);
    }

    private void UpdateOrder()
    {
        //UI Updates here
    }
}
