﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField]
    Transform objRotate;

    // Start is called before the first frame update
    void Start()
    {
        if (!objRotate)
        {
            objRotate = transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float rot = -Input.GetAxis("Horizontal") * 300 * Time.deltaTime;

        Vector3 rotation = objRotate.rotation.eulerAngles + new Vector3(0, 0, rot);
        rotation.z = ClampAngle(rotation.z, -45, 45);
        objRotate.eulerAngles = rotation;
        Debug.DrawRay(objRotate.position, objRotate.up, Color.red);
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < 0f) angle = 360 + angle;
        if (angle > 180f) return Mathf.Max(angle, 360 + min);
        return Mathf.Min(angle, max);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Fruit"))
        {
            Transform curFruit = collision.transform;
            Rigidbody2D rb = curFruit.GetComponent<Rigidbody2D>();

            rb.velocity = new Vector2(0, 0);
            rb.AddForce(transform.up * 100);
            
        }
    }
}
